@extends('layout.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Hover Row</h3>
                            <div class="right">
                                <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"><i class="lnr lnr-plus-circle"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Kode </th>
                                        <th>Mata Pelajaran</th>
                                        <th>Semester</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($mapel as $mata_pelajaran)
                                    <tr>
                                        <td>{{$mata_pelajaran->kode}}</td>
                                        <td>{{$mata_pelajaran->nama}}</td>
                                        <td>{{$mata_pelajaran->semester}}</td>
                                        <td><a href="/mapel/{{$mata_pelajaran->id}}/edit" class="btn btn-warning btn-sm ">edit</a></td>
                                        <td><a href="/mapel/{{$mata_pelajaran->id}}/delete" class="btn btn-danger btn-sm " onclick="return confirm('apa anda yakin menghapus data ini')"
                                                >hapus</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Input Data Pelajaran</h2>
            </div>
            <div class="modal-body">
                <form action="/mapel/create" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kode </label>
                        <input name="kode" type="text" class="form-control" id="kode" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mata Pelajaran </label>
                        <input name="nama" type="text" class="form-control" id="nama" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Semester</label>
                        <select name="semester" class="form-control" id="semester">
                        <option value="Ganjil">Ganjil</option>
                        <option value="Genap">Genap</option>
                        </select>
                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
        </div>
    </div>
</div>
@endsection


