@extends('layout.master')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h2 class="panel-title">edit mata pelajaran</h2>
                        </div>
                        <div class="panel-body">
                            {{-- perintah enctype untuk upload foto --}}
                            <form action="/mapel/{{$mapel->id}}/update" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                  <label for="exampleInputEmail1">kode</label>
                                <input name="kode" type="text" class="form-control" id="kode" aria-describedby="emailHelp" value="{{$mapel->kode}}">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Mata pelajaran</label>
                                  <input name="nama" type="text" class="form-control" id="nama" aria-describedby="emailHelp" value="{{$mapel->nama}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Jenis kelamin</label>
                                    <select name="jenis_kelamin" class="form-control" id="jenis_kelamin">
                                      <option value="Ganjil" @if($mapel->semester == 'Ganjil') selected @endif>Ganjil</option>
                                      <option value="Genap" @if($mapel->semester == 'Genap') selected @endif>Genap</option>
                                    </select>
                                  </div>
                                <button type="submit" class="btn btn-warning">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop

