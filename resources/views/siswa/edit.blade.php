@extends('layout.master')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h2 class="panel-title">Form edit data siswa</h2>
                        </div>
                        <div class="panel-body">
                            {{-- perintah enctype untuk upload foto --}}
                            <form action="/siswa/{{$siswa->id}}/update" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Nama Depan</label>
                                <input name="nama_depan" type="text" class="form-control" id="nama_depan" aria-describedby="emailHelp" value="{{$siswa->nama_depan}}">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Nama Belakang</label>
                                  <input name="nama_belakang" type="text" class="form-control" id="nama_belakang" aria-describedby="emailHelp" value="{{$siswa->nama_belakang}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Jenis kelamin</label>
                                    <select name="jenis_kelamin" class="form-control" id="jenis_kelamin">
                                      <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif>Laki-Laki</option>
                                      <option value="P" @if($siswa->jenis_kelamin == 'P') selected @endif>Perempuan</option>
                                    </select>
                                  </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Agama</label>
                                    <input name="agama" type="text" class="form-control" id="agama" aria-describedby="emailHelp "value="{{$siswa->agama}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Alamat</label>
                                    <textarea name="alamat" class="form-control" id="alamat" rows="3" >{{$siswa->alamat}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Avatar</label>
                                    <input type="file" name="avatar" class="form-control">

                                </div>
                                <button type="submit" class="btn btn-warning">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop

