<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
</a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
            <a class="js-scroll-trigger" href="/login">Masuk</a>
            </li>
            <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="/home">Beranda</a>
            </li>
            <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="/about">Tentang</a>
            </li>
            <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#">Forum Alumni</a>
            </li>
            <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="/recent">Kegiatan</a>
            </li>
        </ul>
    </nav>
