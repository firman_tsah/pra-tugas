<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Stylish Portfolio - Start Bootstrap Template</title>

  <!-- Bootstrap Core CSS -->
  <link href="{{asset('front/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="{{asset('front/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
  <link href="{{asset('front/vendor/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
  <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>

  <!-- Custom CSS -->
  <link href="{{asset('front/css/styles.css')}}" rel="stylesheet" />
  <link href="{{asset('front/css/stylish-portfolio.min.css')}}" rel="stylesheet">
  <link href="{{asset('front/css/styles-agency.css')}}" rel="stylesheet" />

</head>

<body id="page-top">

  <!-- Navigation -->
@include('layout.frontendheader')

@yield('content')
  <!-- Footer -->
@include('layout.frontendfooter')

<!-- Bootstrap core JavaScript -->
<script src="{{asset('front/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('front/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('front/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('front/js/stylish-portfolio.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="{{asset('front/assets/mail/jqBootstrapValidation.js')}}"></script>
<script src="{{asset('front/assets/mail/contact_me.js')}}"></script>
<script src="{{asset('front/js/scripts.js')}}"></script>
<script src="{{asset('front/js/scripts-agency.js')}}"></script>

</body>
</html>
