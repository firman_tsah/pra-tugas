@extends('layout.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ranking 5 besar</h3>
                                {{-- <div class="right">
                                    <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"><i class="lnr lnr-plus-circle"></i></button>
                                </div> --}}
                            <div class="panel-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Ranking</th>
                                            <th>Nama</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $ranking =1;
                                        @endphp
                                        @foreach ($siswa as $s)
                                        <tr>
                                            <td>{{$ranking}}</td>
                                            <td>{{$s->nama_depan}} {{$s->nama_belakang}}</td>
                                            <td>{{$s->rataRataNilai}}</td>
                                        </tr>
                                        @php
                                            $ranking++;
                                        @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
