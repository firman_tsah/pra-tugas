<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//     // login & regist controller
//     Route::get('/login', 'AuthController@login')->name('login');
//     Route::post('/postlogin', 'AuthController@postlogin');
//     Route::get('/logout', 'AuthController@logout');

// // pembungkus middleware, simpan tutup di akhir route yang akan di lindungi auth, route yg bisa di akses hanya sama admin
//     Route::group(['middleware' => ['auth', 'checkRole:admin']],function () {
//     // data siswa route
//         Route::get('/siswa', 'SiswaController@index');
//         Route::post('/siswa/create', 'SiswaController@create');
//         Route::get('/siswa/{id}/edit', 'SiswaController@edit');
//         Route::post('/siswa/{id}/update', 'SiswaController@update');
//         Route::get('/siswa/{id}/delete', 'SiswaController@delete');
//         Route::get('/siswa/{id}/profile', 'SiswaController@profile');
//     // penutup route m iddelware
//     });

// // route yg bisa di akses dengan admin, siswa
//     Route::group(['middleware' => ['auth', 'checkRole:admin,siswa']],function () {
//     // home route
//         Route::get('/home', 'HomeController@home');

//     });


    // login & regist controller
    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/postlogin', 'AuthController@postlogin');
    Route::get('/logout', 'AuthController@logout');

// pembungkus middleware, simpan tutup di akhir route yang akan di lindungi auth, route yg bisa di akses hanya sama admin
    Route::group(['middleware' => ['auth', 'CheckRole:admin']],function () {
    // Route::group(['middleware' =>'auth'],function(){
    // data siswa route
    Route::get('/siswa', 'SiswaController@index');
    Route::post('/siswa/create', 'SiswaController@create');
    Route::get('/siswa/{id}/edit', 'SiswaController@edit');
    Route::post('/siswa/{id}/update', 'SiswaController@update');
    Route::get('/siswa/{id}/delete', 'SiswaController@delete');
    Route::get('/siswa/{id}/profile', 'SiswaController@profile');
    // route tambah nilai di form sisa
    Route::post('/siswa/{id}/addnilai', 'SiswaController@addnilai');
    // penutup route middelware
    // route menghapus nilai pada profile siswa
    Route::get('/siswa/{id}/{idmapel}/deletenilai', 'SiswaController@deletenilai');

    // route mata pelajaran
    Route::get('/mapel', 'MapelController@index');
    Route::post('/mapel/create', 'MapelController@create');
    Route::get('/mapel/{id}/edit', 'MapelController@edit');
    Route::post('/mapel/{id}/update', 'MapelController@update');
    Route::get('/mapel/{id}/delete', 'MapelController@delete');

    // route data guru
    Route::get('/guru/{id}/profile', 'GuruController@profile');



});
Route::group(['middleware' => ['auth', 'CheckRole:admin,siswa']],function () {

    Route::get('/dashboard', 'DashboardController@dashboard');
    Route::get('/home', 'SiteController@home');
    Route::get('/about', 'SiteController@about');
    Route::get('/recent', 'SiteController@recent');
    Route::get('/register', 'SiteController@register');

});
