<?php

namespace App\Http\Controllers;

use Illuseuminate\Http\Request;
use App\Siswa;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $siswa = Siswa::all();
        $siswa-> map(function($s){
            $s->rataRataNilai = $s->rataRataNilai();
            return$s;
        });
        $siswa = $siswa->sortByDesc('rataRataNilai')->take(5);
        return view('dashboard', ['siswa' => $siswa]);
    }
}
