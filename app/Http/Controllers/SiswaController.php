<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Siswa;
use App\Mapel;
use App\User;

class SiswaController extends Controller
{
    public function dashboard()
    {
        return view('dashboard');
    }

    // form data siswa
    // penambahan $request variabel untuk fungsi search
    public function index(Request $request)
    {
        // dd($request->all());
        if($request->has('cari')){
                $data_siswa =Siswa::where('nama_depan', 'LIKE', '%'.$request->cari.'$')->get();
            }else{
                $data_siswa =Siswa::all();
            }
        return view('siswa.index',['data_siswa' => $data_siswa]);

    }

    //     // menambahkan dua fungsi, ketika admin create data siswa, admin sekaligus meregist siswa sekaligus agar bisa login sebagai siswa bukan sebagai admin
    public function create(Request $request)
    {
    //     // data di input ke tabel user, unutk regist login
        $user = new User;
        $user->role = 'siswa';
        $user->name = $request->nama_depan;
        $user->email = $request->email;
        $user->password = bcrypt('123456789');
    //     // password di deafult ke '123456789' untuk kemudian nanti di rubah sama siswa setelah login
        $user->remember_token = Str::random(60);
    //     // str_random di ganti dengan Str::random
        $user->save();

    //     // data di input ke tabel siswa, untuk data pada tabel siswa
        $request->request->add(['user_id' => $user->id]);
        $siswa = Siswa::create($request->all());
        return redirect('/siswa')->with('berhasil', 'data berhasil di input');
    }

        // create profile biasa
        // public function create(Request $request)
        // {
        //     Siswa::create($request->all());
        //     return redirect('/siswa')->with('berhasil', 'data berhasil di input');
        // }

    public function edit($id)
    {
        $siswa =Siswa::find($id);
        return view('siswa/edit', ['siswa' => $siswa]);
    }

        // di tamba lagi edit untuk upload foto, edit di mulai dari if
        public function update(Request $request, $id)
    {
        // dd($request->all());
        $siswa =Siswa::find($id);
        $siswa->update($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/', $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('/siswa')->with('berhasil', 'data berhasil di update');
    }

    public function delete($id)
    {
        $siswa =Siswa::find($id);
        $siswa->delete($siswa);
        return redirect('/siswa')->with('berhasil', 'data berhasil di hapus');
    }

    public function profile($id)
    {
        // $siswa = Siswa::find($id);
        // return view('siswa.profile', ['siswa' => $siswa]);

        // setelah di tambah perintah untuk menapilkan list mapale padap form isi nilai mapel di profile
        $siswa = Siswa::find($id);
        $matapelajaran = Mapel::all();
        // dd($mapel);
        return view('siswa.profile', ['siswa' => $siswa ,'matapelajaran' => $matapelajaran]);
    }

    // contoller untuk memasukan nilai pada form siswa
    public function addnilai(Request $request,$id)
    {
        $siswa =Siswa::find($id);
        if($siswa->mapel()->where('mapel_id' , $request->mapel)->exists()){
            return redirect()->back()->with('gagal', 'data mata pelajaran sudah ada');
        }
        $siswa->mapel()->attach($request->mapel,['nilai' => $request->nilai]);

        return redirect()->back()->with('berhasil', 'data nilai berhasil di input');
    }

    // controller untuk menghapus data nilai yang ada pada profil siswa
    public function deletenilai($idsiswa,$idmapel)
    {
        $siswa =Siswa::find($idsiswa);
        $siswa->mapel()->detach($idmapel);
        return redirect()->back()->with('berhasil', 'data berhasil di hapus');
    }
}
