<?php

namespace App\Http\Controllers;
use App\Mapel;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    public function index()
    {
        $mapel= Mapel::all();
        return view('mapel.index', ['mapel' => $mapel]);
    }

    public function create(Request $request)
    {
        Mapel::create($request->all());
        return redirect('/mapel');
    }

    public function edit($id)
    {
        $mapel =Mapel::find($id);
        return view('mapel.edit', ['mapel' => $mapel]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $mapel =Mapel::find($id);
        $mapel->update($request->all());
        return redirect('/mapel')->with('berhasil', 'data berhasil di update');
    }

    public function delete($id)
    {
        $mapel =Mapel::find($id);
        $mapel->delete($mapel);
        return redirect('/mapel')->with('berhasil', 'data berhasil di hapus');
    }
}
