<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = [
                            'nama_depan',
                            'nama_belakang',
                            'jenis_kelamin',
                            'agama',
                            'alamat',
                            'avatar',
                            'user_id'];

    public function getAvatar()
    {
        if(!$this->avatar){
            return asset('images/default.jpg');
        }
        return asset('images/' .$this->avatar);
    }

    // relation many to many mapel dengan siswa
    public function mapel()
    {
        // di tambahkan withpivot untuk menampilkan nilai pada kolom nilai yang tertera pada tabel pivot
        return $this->belongsToMany(Mapel::class)->withPivot(['nilai'])->withTimestamps();
    }

    public function rataRataNilai()
    {
        $total = 0;
        $hitung = 0;
        foreach($this->mapel as $mapel){
            $total += $mapel->pivot->nilai;
            $hitung++;
        }
        // round pada return berfungsi membulatkan bilangan
        return round($total/$hitung);
    }
}

