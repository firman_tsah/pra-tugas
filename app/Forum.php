<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $table = 'forum';
    public function user()
    {
        return $this->belongsTo(user::class);
    }

    // relasi (forum punya banayk komentar)
    public function komentar()
    {
        return $this->belongsToMany(komentar::class);
    }

}
