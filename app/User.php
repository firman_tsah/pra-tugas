<?php
namespace App;

use App\forum;
use App\komentar;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // relasi one to many (setiap user memiliki banyak forum)
    public function forum()
    {
        return $this->hasMany(forum::class);
    }

    // relasi one to many (setiap user memiliki banyak komentar)
    public function komentar()
    {
        return $this->hasMany(komentar::class);
    }
}
