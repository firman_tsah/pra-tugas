<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'mapel';
    protected $fillable = [
                            'kode',
                            'nama',
                            'semester',];

    // relation many to many mapel dengan siswa
    public function siswa()
    {
        // di tambahkan withpivot untuk menampilkan nilai pada kolom nilai yang tertera pada tabel pivot
        return $this->belongsToMany(Siswa::class)->withPivot(['nilai']);
    }

    public function guru()
    {
        return $this->belongsTo(Guru::class);
    }

}
